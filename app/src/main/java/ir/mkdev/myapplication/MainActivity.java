package ir.mkdev.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import ir.mkdev.myapplication.model.StepItem;
import ir.mkdev.myapplication.model.TemplateModel;
import ir.mkdev.myapplication.model.TypesItem;
import ir.mkdev.myapplication.model.element.ActionsItem;
import ir.mkdev.myapplication.model.element.ElementsItem;
import ir.mkdev.myapplication.utils.OnSwipeTouchListener;
import ir.mkdev.myapplication.utils.UiUtils;

public class MainActivity extends AppCompatActivity {

    List<StepItem> steps = null;
    private FrameLayout frameLayout;
    private int stepIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = findViewById(R.id.frame);
    }

    public void runDesign(View view) {
        stepIndex = -1;

        String jsonString = UiUtils.readJson(this, R.raw.template);
        TemplateModel templateModel = new Gson().fromJson(jsonString, TemplateModel.class);
        steps = templateModel.getSteps();

        prepareStep();
    }

    private void parseElementObject(FrameLayout container, ElementsItem elementsItem) {
        if (elementsItem.getType() == null) {
            return;
        }
        switch (elementsItem.getType()) {
            case BUTTON:
                addButton(container, elementsItem);
                break;
            case IMAGE_VIEW:
                addImageView(container, elementsItem);
                break;
            case TEXT_VIEW:
                addTextView(container, elementsItem);
                break;
            case VIDEO_VIEW:
                addVideoView(container, elementsItem);
                break;
            case RATING_BAR:
                addRatingBar(container, elementsItem);
                break;
            case PROGRESS:
                //todo add this type
                break;
            case VIEW_GROUP:
                addViewGroup(container, elementsItem);
                break;
            case UNKNOWN:

                break;
        }
    }

    private void setPublicAttribute(View container, View view, final ElementsItem elementsItem) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);

        view.setId(elementsItem.getId());

        if (elementsItem.getPosition().getPositionX() != -1) {
            int positionX = (container.getWidth() * elementsItem.getPosition().getPositionX()) / 100;
            view.setTranslationX(positionX);
        }

        if (elementsItem.getPosition().getPositionY() != -1) {
            int positionY = ((container.getHeight() * elementsItem.getPosition().getPositionY()) / 100);
            view.setTranslationY(positionY);
        }

        if (elementsItem.getDimensions().getWidth() != -1) {
            params.width = (container.getWidth() * elementsItem.getDimensions().getWidth()) / 100;
        }

        if (elementsItem.getDimensions().getHeight() != -1) {
            params.height = (container.getHeight() * elementsItem.getDimensions().getHeight()) / 100;
        }

        // Low level priority
        if (!UiUtils.isNullOrEmpty(elementsItem.getAlignment().getLayoutGravity())) {
            params.gravity = UiUtils.getGravity(elementsItem.getAlignment().getLayoutGravity());
        }

        if (!UiUtils.isNullOrEmpty(elementsItem.getColor().getBackgroundColor())) {
            view.setBackgroundColor(UiUtils.getColor(elementsItem.getColor().getBackgroundColor()));
        }

        if (elementsItem.getDimensions().getMinWidth() != -1) {
            view.setMinimumWidth(elementsItem.getDimensions().getMinWidth());
        }

        if (elementsItem.getDimensions().getMinHeight() != -1) {
            view.setMinimumHeight(elementsItem.getDimensions().getMinHeight());
        }

        if (elementsItem.isClickable()) {
            handelEvent(view, elementsItem);
        }
        view.setLayoutParams(params);
    }

    private void setTextViewAttribute(View container, TextView view, ElementsItem elementsItem) {
        view.setText(elementsItem.getText().getText());
        view.setTextSize(elementsItem.getText().getTextSize());
        view.setTextColor(UiUtils.getColor(elementsItem.getColor().getTextColor()));
        view.setTypeface(view.getTypeface(), UiUtils.getTextStyle(elementsItem.getText().getTextStyle()));

        if (elementsItem.getColor().getTransparency() >= 0 && elementsItem.getColor().getTransparency() <= 1) {
            view.setAlpha(elementsItem.getColor().getTransparency());
        }

        if (!UiUtils.isNullOrEmpty(elementsItem.getAlignment().getGravity())) {
            view.setGravity(UiUtils.getGravity(elementsItem.getAlignment().getGravity()));
        }

        if (elementsItem.getDimensions().getMaxWidth() != -1) {
            view.setMaxWidth(elementsItem.getDimensions().getMaxWidth());
        }

        if (elementsItem.getDimensions().getMaxHeight() != -1) {
            view.setMaxHeight(elementsItem.getDimensions().getMaxHeight());
        }

        if (elementsItem.getAlignment().getDirection().equals("ltr")) {
            view.setTextDirection(View.TEXT_DIRECTION_LTR);
        } else if (elementsItem.getAlignment().getDirection().equals("rtl")) {
            view.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
    }

    private void addTextView(FrameLayout container, ElementsItem elementsItem) {
        TextView view = new TextView(this);
        setPublicAttribute(container, view, elementsItem);
        setTextViewAttribute(container, view, elementsItem);

        container.addView(view);
    }

    private void addButton(FrameLayout container, ElementsItem elementsItem) {
        Button view = new Button(this);
        view.setAllCaps(false);
        setPublicAttribute(container, view, elementsItem);
        setTextViewAttribute(container, view, elementsItem);

        container.addView(view);
    }

    private void addRatingBar(FrameLayout container, ElementsItem elementsItem) {
        RatingBar view = new RatingBar(this, null, android.R.attr.ratingBarStyleSmall);
        setPublicAttribute(container, view, elementsItem);
        view.setNumStars(elementsItem.getRatingBar().getNumStars());
        view.setRating(elementsItem.getRatingBar().getRating());

        container.addView(view);
    }

    private void addImageView(FrameLayout container, ElementsItem elementsItem) {
        ImageView view = new ImageView(this);
        setPublicAttribute(container, view, elementsItem);

        if (elementsItem.getColor().getTransparency() >= 0 && elementsItem.getColor().getTransparency() <= 1) {
            view.setAlpha(255 * elementsItem.getColor().getTransparency()); //value: [0-255]. Where 0 is fully transparent and 255 is fully opaque.
        }

        if (elementsItem.getDimensions().getMaxWidth() != -1) {
            view.setMaxWidth(elementsItem.getDimensions().getMaxWidth());
        }

        if (elementsItem.getDimensions().getMaxHeight() != -1) {
            view.setMaxHeight(elementsItem.getDimensions().getMaxHeight());
        }

        if (elementsItem.getScaleType() != null) {
            view.setScaleType(UiUtils.getImageViewScaleType(elementsItem.getScaleType()));
        }

        if (!UiUtils.isNullOrEmpty(elementsItem.getColor().getBackgroundImage())) {
            Glide.with(this).load(elementsItem.getColor().getBackgroundImage()).into(view);
        }

        container.addView(view);
    }

    private void addVideoView(FrameLayout container, ElementsItem elementsItem) {
        VideoView view = new VideoView(this);
        setPublicAttribute(container, view, elementsItem);

        //MediaController mediaController = new MediaController(this);
        //mediaController.setAnchorView(view);
        //view.setMediaController(mediaController);
        view.setVideoPath(elementsItem.getSrc());
        view.requestFocus();
        if (elementsItem.isAutoPlay()) {
            view.start();
        }

        container.addView(view);
    }

    private void addViewGroup(final FrameLayout container, final ElementsItem elementsItem) {
        final FrameLayout frame = new FrameLayout(this);
        setPublicAttribute(container, frame, elementsItem);
        container.addView(frame);

        container.post(new Runnable() {
            @Override
            public void run() {
                List<ElementsItem> elementsList = elementsItem.getElements();
                if (elementsList == null) {
                    return;
                }
                for (ElementsItem item : elementsList) {
                    parseElementObject(frame, item);
                }
            }
        });
    }

    private void handelEvent(View view, final ElementsItem elementsItem) {
        if (elementsItem.getActions() == null) {
            return;
        }
        for (final ActionsItem actionsItem : elementsItem.getActions()) {
            if (actionsItem.getEvent() == null) {
                break;
            }
            switch (actionsItem.getEvent().getName()) {
                case CLICK:
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Toast.makeText(MainActivity.this, "CLICK", Toast.LENGTH_SHORT).show();
                            handelAction(actionsItem);
                        }
                    });
                    break;
                case LONG_CLICK:
                    view.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            Toast.makeText(MainActivity.this, "LONG CLICK", Toast.LENGTH_SHORT).show();

                            handelAction(actionsItem);
                            return true;
                        }
                    });
                    break;
                case SWIPE:
                    view.setOnTouchListener(new OnSwipeTouchListener(this) {
                        public void onSwipeTop() {
                            Toast.makeText(MainActivity.this, "TOP", Toast.LENGTH_SHORT).show();
                        }

                        public void onSwipeRight() {
                            Toast.makeText(MainActivity.this, "RIGHT", Toast.LENGTH_SHORT).show();
                        }

                        public void onSwipeLeft() {
                            Toast.makeText(MainActivity.this, "LEFT", Toast.LENGTH_SHORT).show();
                        }

                        public void onSwipeBottom() {
                            Toast.makeText(MainActivity.this, "BOTTOM", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
            }
        }
    }

    private void handelAction(ActionsItem actionsItem) {
        if (actionsItem.getType() == null) {
            return;
        }
        switch (actionsItem.getType()) {
            case OPEN_LINK:
                openLinkAction(actionsItem.getSrc());
                break;
            case OPEN_INTENT:
                Toast.makeText(this, "Action OPEN INTENT", Toast.LENGTH_SHORT).show();
                break;
            case SHOW_HIDE:
                showHideViewAction(actionsItem.getViewIds());
                break;
            case PLAY_VIDEO:
                Toast.makeText(this, "Action PLAY VIDEO", Toast.LENGTH_SHORT).show();
                break;
            case CLOSE:
                Toast.makeText(this, "ACTION CLOSE", Toast.LENGTH_SHORT).show();
                break;
            case REPORT:
                Toast.makeText(this, "ACTION REPORT", Toast.LENGTH_SHORT).show();
                break;
            case SLIDE_UP_DOWN:
                slideUpDownViewAction(actionsItem);
                break;
            case NEXT_STEP:
                prepareStep();
                break;
            case UNKNOWN:

                break;
        }
    }

    private void prepareStep() {
        stepIndex++;
        if (stepIndex >= steps.size()) {
            Toast.makeText(this, "FINISH STEPS", Toast.LENGTH_SHORT).show();
            return;
        }

        frameLayout.removeAllViews();

        for (TypesItem typesItem : steps.get(stepIndex).getTypes()) {
            if (typesItem.getPhoneType() == UiUtils.getPhoneType(this) &&
                    typesItem.getPhoneOrientation() == UiUtils.getScreenOrientation(this)) {
                for (ElementsItem elementsItem : typesItem.getElements()) {
                    parseElementObject(frameLayout, elementsItem);
                }
            }
        }
    }

    private void showHideViewAction(List<Integer> ids) {
        if (ids == null) {
            return;
        }
        List<View> views = UiUtils.getViewsList(this, ids);
        for (View view : views) {
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    private void slideUpDownViewAction(ActionsItem actionItem) {
        if (actionItem == null) {
            return;
        }
        List<View> views = UiUtils.getViewsList(this, actionItem.getViewIds());
        for (View view : views) {

            int down = (view.getHeight() * actionItem.getValue()) / 100;

            if (view.getTranslationY() == 0f) {
                // GO TO DOWN
                view.setTranslationY(down);
            } else {
                // GO TO UP
                view.setTranslationY(0f);
            }
        }
    }

    private void openLinkAction(String src) {
        if (UiUtils.isNullOrEmpty(src)) {
            return;
        }
        String source = src;
        if (!src.startsWith("http://") &&
                !src.startsWith("https://")) {
            source = "http://".concat(src);
        }
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(source)));
    }
}
