package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

import ir.mkdev.myapplication.enums.TextStyleEnum;

public class Text {

    @SerializedName("text_style")
    private TextStyleEnum textStyle;

    @SerializedName("text_size")
    private int textSize;

    @SerializedName("text")
    private String text;

    public TextStyleEnum getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyleEnum textStyle) {
        this.textStyle = textStyle;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public String getText() {
        return text == null ? "" : text;
    }

    public void setText(String text) {
        this.text = text;
    }
}