package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.mkdev.myapplication.enums.ActionTypeEnum;

public class ActionsItem {

    @SerializedName("view_ids")
    private List<Integer> viewIds;

    @SerializedName("src")
    private String src;

    @SerializedName("type")
    private ActionTypeEnum type;

    @SerializedName("event")
    private Event event;

    @SerializedName("value")
    private int value;

    public List<Integer> getViewIds() {
        return viewIds;
    }

    public void setViewIds(List<Integer> viewIds) {
        this.viewIds = viewIds;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ActionTypeEnum getType() {
        return type;
    }

    public void setType(ActionTypeEnum type) {
        this.type = type;
    }
}