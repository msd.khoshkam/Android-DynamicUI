package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

public class Dimensions {

    @SerializedName("max_width")
    private int maxWidth;

    @SerializedName("min_height")
    private int minHeight;

    @SerializedName("width")
    private int width;

    @SerializedName("max_height")
    private int maxHeight;

    @SerializedName("height")
    private int height;

    @SerializedName("min_width")
    private int minWidth;

    public int getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public int getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(int minHeight) {
        this.minHeight = minHeight;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(int minWidth) {
        this.minWidth = minWidth;
    }
}