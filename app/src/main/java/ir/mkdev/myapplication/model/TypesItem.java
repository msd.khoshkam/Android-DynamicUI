package ir.mkdev.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.mkdev.myapplication.enums.PhoneTypeEnum;
import ir.mkdev.myapplication.enums.ScreenOrientationeEnum;
import ir.mkdev.myapplication.model.element.ElementsItem;

public class TypesItem {

    @SerializedName("elements")
    private List<ElementsItem> elements;

    @SerializedName("phone_type")
    private PhoneTypeEnum phoneType;

    @SerializedName("phone_orientation")
    private ScreenOrientationeEnum phoneOrientation;

    public List<ElementsItem> getElements() {
        return elements;
    }

    public void setElements(List<ElementsItem> elements) {
        this.elements = elements;
    }

    public PhoneTypeEnum getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(PhoneTypeEnum phoneType) {
        this.phoneType = phoneType;
    }

    public ScreenOrientationeEnum getPhoneOrientation() {
        return phoneOrientation;
    }

    public void setPhoneOrientation(ScreenOrientationeEnum phoneOrientation) {
        this.phoneOrientation = phoneOrientation;
    }
}