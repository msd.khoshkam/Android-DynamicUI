package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

public class RatingBar{

	@SerializedName("num_stars")
	private int numStars;

	@SerializedName("rating")
	private int rating;

	public int getNumStars() {
		return numStars;
	}

	public void setNumStars(int numStars) {
		this.numStars = numStars;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
}