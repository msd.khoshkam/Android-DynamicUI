package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.mkdev.myapplication.enums.ElementTypeEnum;
import ir.mkdev.myapplication.enums.ImageScaleTypeEnum;

public class ElementsItem {

    @SerializedName("color")
    private Color color;

    @SerializedName("src")
    private String src;

    @SerializedName("clickable")
    private boolean clickable;

    @SerializedName("auto_play")
    private boolean autoPlay;

    @SerializedName("type")
    private ElementTypeEnum type;

    @SerializedName("rating_bar")
    private RatingBar ratingBar;

    @SerializedName("elements")
    private List<ElementsItem> elements;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("text")
    private Text text;

    @SerializedName("position")
    private Position position;

    @SerializedName("alignment")
    private Alignment alignment;

    @SerializedName("actions")
    private List<ActionsItem> actions;

    @SerializedName("scale_type")
    private ImageScaleTypeEnum scaleType;

    @SerializedName("dimensions")
    private Dimensions dimensions;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public boolean isAutoPlay() {
        return autoPlay;
    }

    public void setAutoPlay(boolean autoPlay) {
        this.autoPlay = autoPlay;
    }

    public ElementTypeEnum getType() {
        return type;
    }

    public void setType(ElementTypeEnum type) {
        this.type = type;
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }

    public void setRatingBar(RatingBar ratingBar) {
        this.ratingBar = ratingBar;
    }

    public List<ElementsItem> getElements() {
        return elements;
    }

    public void setElements(List<ElementsItem> elements) {
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public List<ActionsItem> getActions() {
        return actions;
    }

    public void setActions(List<ActionsItem> actions) {
        this.actions = actions;
    }

    public ImageScaleTypeEnum getScaleType() {
        return scaleType;
    }

    public void setScaleType(ImageScaleTypeEnum scaleType) {
        this.scaleType = scaleType;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }
}