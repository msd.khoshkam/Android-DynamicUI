package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

public class Color {

    @SerializedName("background_image")
    private String backgroundImage;

    @SerializedName("background_color")
    private String backgroundColor;

    @SerializedName("color")
    private String color;

    @SerializedName("transparency")
    private float transparency;// min 0- max 1

    @SerializedName("text_color")
    private String textColor;

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getTransparency() {
        return transparency;
    }

    public void setTransparency(float transparency) {
        this.transparency = transparency;
    }

    public String getTextColor() {
        return textColor == null ? "#FF000000" : textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }
}