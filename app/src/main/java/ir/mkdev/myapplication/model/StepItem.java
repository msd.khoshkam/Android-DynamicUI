package ir.mkdev.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StepItem {

    @SerializedName("name")
    private String name;

    @SerializedName("types")
    private List<TypesItem> types;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TypesItem> getTypes() {
        return types;
    }

    public void setTypes(List<TypesItem> types) {
        this.types = types;
    }
}