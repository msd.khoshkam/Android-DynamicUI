package ir.mkdev.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TemplateModel {

    @SerializedName("steps")
    private List<StepItem> steps;

    @SerializedName("version")
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<StepItem> getSteps() {
        return steps;
    }

    public void setSteps(List<StepItem> steps) {
        this.steps = steps;
    }
}