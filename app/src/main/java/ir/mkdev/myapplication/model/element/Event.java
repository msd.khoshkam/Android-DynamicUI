package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

import ir.mkdev.myapplication.enums.EventTypeEnum;

public class Event{

	@SerializedName("name")
	private EventTypeEnum name;

	@SerializedName("params")
	private String params;

	public EventTypeEnum getName() {
		return name;
	}

	public void setName(EventTypeEnum name) {
		this.name = name;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
}