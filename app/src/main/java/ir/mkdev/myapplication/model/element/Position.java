package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

public class Position{

	@SerializedName("position_x")
	private int positionX;

	@SerializedName("position_y")
	private int positionY;

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
}