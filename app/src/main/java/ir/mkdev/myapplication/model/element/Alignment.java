package ir.mkdev.myapplication.model.element;

import com.google.gson.annotations.SerializedName;

public class Alignment {

    @SerializedName("gravity")
    private String gravity;

    @SerializedName("layout_gravity")
    private String layoutGravity;

    @SerializedName("direction")
    private String direction;

    public String getGravity() {
        return gravity;
    }

    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

    public String getLayoutGravity() {
        return layoutGravity;
    }

    public void setLayoutGravity(String layoutGravity) {
        this.layoutGravity = layoutGravity;
    }

    public String getDirection() {
        return direction == null ? "" : direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}