package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum PhoneTypeEnum {

    @SerializedName("mobile")
    MOBILE,

    @SerializedName("tablet")
    TABLET,

    UNKNOWN
}
