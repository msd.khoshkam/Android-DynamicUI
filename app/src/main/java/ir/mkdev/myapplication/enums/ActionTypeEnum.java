package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum ActionTypeEnum {

    @SerializedName("open_link")
    OPEN_LINK,

    @SerializedName("open_intent")
    OPEN_INTENT,

    @SerializedName("play_video")
    PLAY_VIDEO,

    @SerializedName("close")
    CLOSE,

    @SerializedName("report")
    REPORT,

    @SerializedName("slide_up_down")
    SLIDE_UP_DOWN,

    @SerializedName("show_hide")
    SHOW_HIDE,

    @SerializedName("next_step")
    NEXT_STEP,

    UNKNOWN
}
