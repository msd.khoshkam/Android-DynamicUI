package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum EventTypeEnum {

    @SerializedName("click")
    CLICK,

    @SerializedName("long_click")
    LONG_CLICK,

    @SerializedName("swipe")
    SWIPE,

    @SerializedName("video_progress")
    VIDEO_PROGRESS,

    UNKNOWN
}
