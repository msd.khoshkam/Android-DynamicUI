package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum ScreenOrientationeEnum {

    @SerializedName("orientation_portrait")
    ORIENTATION_PORTRAIT,

    @SerializedName("orientation_landscape")
    ORIENTATION_LANDSCAPE,

    UNKNOWN
}
