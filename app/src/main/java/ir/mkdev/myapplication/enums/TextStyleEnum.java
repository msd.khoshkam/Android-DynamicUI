package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum TextStyleEnum {

    @SerializedName("bold_italic")
    BOLD_ITALIC,

    @SerializedName("bold")
    BOLD,

    @SerializedName("italic")
    ITALIC,

    @SerializedName("normal")
    NORMAL,

    UNKNOWN
}
