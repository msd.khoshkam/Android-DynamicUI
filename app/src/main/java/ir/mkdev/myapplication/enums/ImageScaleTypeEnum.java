package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum ImageScaleTypeEnum {

    @SerializedName("matrix")
    MATRIX,

    @SerializedName("fit_xy")
    FIT_XY,

    @SerializedName("fit_start")
    FIT_START,

    @SerializedName("fit_center")
    FIT_CENTER,

    @SerializedName("fit_end")
    FIT_END,

    @SerializedName("center")
    CENTER,

    @SerializedName("center_crop")
    CENTER_CROP,

    @SerializedName("center_inside")
    CENTER_INSIDE,

    UNKNOWN
}
