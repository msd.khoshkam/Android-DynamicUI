package ir.mkdev.myapplication.enums;

import com.google.gson.annotations.SerializedName;

public enum ElementTypeEnum {

    @SerializedName("button")
    BUTTON,

    @SerializedName("image_view")
    IMAGE_VIEW,

    @SerializedName("text_view")
    TEXT_VIEW,

    @SerializedName("video_view")
    VIDEO_VIEW,

    @SerializedName("rating_bar")
    RATING_BAR,

    @SerializedName("progress")
    PROGRESS,

    @SerializedName("view_group")
    VIEW_GROUP,

    UNKNOWN
}
