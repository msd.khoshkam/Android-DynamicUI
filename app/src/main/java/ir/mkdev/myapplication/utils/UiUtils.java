package ir.mkdev.myapplication.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import ir.mkdev.myapplication.enums.ImageScaleTypeEnum;
import ir.mkdev.myapplication.enums.PhoneTypeEnum;
import ir.mkdev.myapplication.enums.ScreenOrientationeEnum;
import ir.mkdev.myapplication.enums.TextStyleEnum;

public class UiUtils {

    public static DisplayMetrics getDimension(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String readJson(Context context, int resource) {
        InputStream is = context.getResources().openRawResource(resource);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static int getActionBarHeight(Context context) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static float convertDpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static float convertPxToDp(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static int getGravity(String value) {
        int gravity = 0;

        String[] split = value.split(",");
        for (String str : split) {
            switch (str.trim().toLowerCase()) {
                case "left":
                    gravity = gravity | Gravity.START;
                    break;
                case "right":
                    gravity = gravity | Gravity.END;
                    break;
                case "top":
                    gravity = gravity | Gravity.TOP;
                    break;
                case "bottom":
                    gravity = gravity | Gravity.BOTTOM;
                    break;
                case "center":
                    gravity = gravity | Gravity.CENTER;
                    break;
                case "center_vertical":
                    gravity = Gravity.BOTTOM | Gravity.CENTER_VERTICAL;
                    break;
                case "center_horizontal":
                    gravity = gravity | Gravity.CENTER_HORIZONTAL;
                    break;
            }
        }
        return gravity;
    }

    public static Boolean isNullOrEmpty(String value) {
        return value == null || value.isEmpty() || value.equals("null");
    }

    public static int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception e) {
            return Color.parseColor("FF000000");
        }
    }

    public static int getTextStyle(TextStyleEnum value) {
        if (value == null) {
            return Typeface.NORMAL;
        }
        switch (value) {
            case BOLD_ITALIC:
                return Typeface.BOLD_ITALIC;
            case BOLD:
                return Typeface.BOLD;
            case ITALIC:
                return Typeface.ITALIC;
            case NORMAL:
                return Typeface.NORMAL;
            default:
                return Typeface.NORMAL;
        }
    }

    public static List<View> getViewsList(Activity activity, List<Integer> ids) {
        List<View> view = new ArrayList<>();
        for (int resID : ids) {
            try {
                view.add(activity.findViewById(resID));
            } catch (Exception e) {
                Log.e(activity.getClass().getSimpleName(), "View id is not valid!");
            }
        }

        return view;
    }

    public static ScreenOrientationeEnum getScreenOrientation(Context context) {
        int screenOrientation = context.getResources().getConfiguration().orientation;
        if (screenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            return ScreenOrientationeEnum.ORIENTATION_PORTRAIT;
        } else {
            return ScreenOrientationeEnum.ORIENTATION_LANDSCAPE;
        }
    }

    public static PhoneTypeEnum getPhoneType(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null && manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            return PhoneTypeEnum.TABLET;
        } else {
            return PhoneTypeEnum.MOBILE;
        }
    }

    public static ImageView.ScaleType getImageViewScaleType(ImageScaleTypeEnum value) {
        if (value == null) {
            return ImageView.ScaleType.CENTER;
        }
        switch (value) {
            case MATRIX:
                return ImageView.ScaleType.MATRIX;
            case FIT_XY:
                return ImageView.ScaleType.FIT_XY;
            case FIT_START:
                return ImageView.ScaleType.FIT_START;
            case FIT_CENTER:
                return ImageView.ScaleType.FIT_CENTER;
            case FIT_END:
                return ImageView.ScaleType.FIT_END;
            case CENTER:
                return ImageView.ScaleType.CENTER;
            case CENTER_CROP:
                return ImageView.ScaleType.CENTER_CROP;
            case CENTER_INSIDE:
                return ImageView.ScaleType.CENTER_INSIDE;
            default:
                return ImageView.ScaleType.CENTER;
        }
    }
}
